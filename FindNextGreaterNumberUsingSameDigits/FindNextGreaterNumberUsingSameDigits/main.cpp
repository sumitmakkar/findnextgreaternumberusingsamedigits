#include <iostream>
#include <vector>

using namespace std;

class Engine
{
    private:
        string      numberStr;
        vector<int> numericVector;
    
        void populateNumericVector()
        {
            //int len = (int)numberStr.length();
            for(int i = 0 ; i < 10 ; i++)
            {
                numericVector.push_back(0);
            }
        }
    
    public:
        Engine(string nStr)
        {
            numberStr = nStr;
            populateNumericVector();
        }
    
        string findNextLargestNumber()
        {
            int len = (int)numberStr.length() - 2;
            numericVector[numberStr[len+1] - '0']++;
            while(len >= 0 && numberStr[len] >= numberStr[len+1])
            {
                numericVector[numberStr[len] - '0']++;
                len--;
            }
            if(len < 0)
            {
                return numberStr;
            }
            numericVector[numberStr[len] - '0']++;
            int vectorLen = (int)numericVector.size();
            for(int i = (numberStr[len] - '0') + 1 ; i < vectorLen ; i++)
            {
                if(numericVector[i])
                {
                    numericVector[i]--;
                    numberStr[len] = i + '0';
                    len++;
                    break;
                }
            }
            for(int i = 0 ; i < vectorLen ; i++)
            {
                while(numericVector[i])
                {
                    numericVector[i]--;
                    numberStr[len] = i + '0';
                    len++;
                }
            }
            return numberStr;
        }
};

int main(int argc, const char * argv[])
{
//    string numberStr = "6938652";
//    string numberStr = "97324852";
    string numberStr = "4321";
    Engine e         = Engine(numberStr);
    cout<<e.findNextLargestNumber()<<endl;
    return 0;
}
